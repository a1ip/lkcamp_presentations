.. role:: cover-title

.. role:: cover

:cover-title:`Device drivers`
#############################

.. space:: 150

.. class:: cover

  Meeting 3

  LKCAMP - Round 4 - Apr 28, 2020

.. raw:: pdf

   PageBreak standardPage


Device Driver
=============

* **A program that operates a device.**
* Interface between hardware and the OS (and any other application accessing
  it).
* Abstracts the details of how the device works.


Driver: from kernel to device
=============================

.. image:: img/diag_prog_to_device.png
   :width: 15cm


So much driver code in the kernel
=================================

.. image:: img/drivers_vs_subsystems.png
   :width: 9cm

Source: https://github.com/satoru-takeuchi/linux-kernel-statistics/


There are always new drivers
============================

.. image:: img/new_vs_old_drivers.png
   :width: 15cm

Source: https://github.com/satoru-takeuchi/linux-kernel-statistics/


.. raw:: pdf

   PageBreak coverPage

:cover-title:`How a device driver works`
========================================

.. raw:: pdf

   PageBreak standardPage


File as an API
==============

* Normally we think of a file as a container of information saved on disk, but
  that's not always true!
* File is an API (an interface for programs).
  The most common actions of that interface are:

  * Open
  * Read
  * Write
  * Close

* Device files can be created to, instead of storing information on disk,
  communicate with the kernel through those actions!


Device files
============

* On UNIX-like systems (like Linux), "Everything is a file".
* Device files can be used to:

  * Query information from the kernel
  * Send information to the kernel
  * Query information from a driver
  * Send information to a driver

* For example, you can get information about your CPU from the kernel by reading
  '/proc/cpuinfo', with ``cat /proc/cpuinfo``.
  That file doesn't exist in your disk!


Major and minor numbers
=======================

* Since device files are used to communicate with the drivers, there must be a
  way to know which files correspond to which drivers: major and minor numbers.
* The major number identifies the driver.
* The minor number identifies the device (a single instance) controlled by the
  driver.


Major and minor example
=======================

.. image:: img/major-minor.png
   :width: 15cm

* These two files represent two video devices, like two cameras.
* Both cameras use the same driver (major 81).
* Each camera is a separate camera (minors 0 and 1).


Device type
===========

* Devices can have one of three types:

  * **Block**: used for raw storage (*eg* disk)
  * **Network**: used for network interfaces
  * **Character**: used for most devices

* The device type changes which file operations can be implemented.


Defining the driver behavior
============================

* The driver needs a way to tell the kernel what happens when a file operation
  is called on a file with its major number.
* This is done by hooking callbacks (function pointers) in ``struct
  file_operations``.


The struct file_operations
==========================

.. code-block:: c

   struct file_operations {
       ssize_t (*read) (struct file *, char __user *, size_t, loff_t *);
       ssize_t (*write) (struct file *, const char __user *, size_t, loff_t *);
       __poll_t (*poll) (struct file *, struct poll_table_struct *);
       int (*mmap) (struct file *, struct vm_area_struct *);
       int (*open) (struct inode *, struct file *);
       int (*flush) (struct file *, fl_owner_t id);
       int (*release) (struct inode *, struct file *);
       int (*lock) (struct file *, int, struct file_lock *);
       ...
   };


Device file creation
====================

* Userspace needs to create device file.
* Some applications that do that:

  * udev
  * mknod


Creating a device file - mknod
==============================

.. image:: img/man-mknod.png
   :width: 15cm

* Example:
  ``$ mknod /dev/my-device c 42 0``


.. raw:: pdf

   PageBreak coverPage

:cover-title:`Modules`
========================================

.. raw:: pdf

   PageBreak standardPage


Why use modules?
================

* With so many different drivers on the Linux kernel, an image containing all of
  them would be wasteful.
* But the image can be compiled containing only the necessary drivers to the
  target system.
* But most users don't want to have to compile their own kernel.
* In this scenario, how can the Linux distros provide a pre-built kernel, with
  all the necessary drivers for the system to function, but without bundling all
  of them, given that each user needs a different combination of drivers?
* Modules!


Solution using modules
======================

* The solution to this problem is to compile each driver as a separate module.
* When the user downloads the Linux kernel pre-built by its preferred distro, it
  comes with all drivers pre-compiled but separate from the main image.
* When the kernel is loaded, only the modules containing the drivers needed for
  the system are loaded.
* Now the user can have a pre-built Linux image that is only big enough for the
  needed modules!


Modules
=======

* A module:

  * is a piece of compiled kernel code;
  * **acts as an extension (plugin) of the kernel**;
  * can be loaded/unloaded without reboot;
  * can be loaded from user space;
  * runs in kernel mode.

* Another benefit of a module is that it can be loaded only while in use: it can
  be loaded when a device is connected and unloaded when disconnected.


Installed modules
=================

* The extension for modules is .ko.
* They are usually located at ``/lib/modules/$(uname -r)/``:

.. image:: img/lib-mod.png
   :width: 7cm


Utility commands
================

* **insmod <file.ko>**:
  Loads module (without dependencies).
* **modprobe <module>**:
  Loads module from /lib/modules/$(uname -r) and its dependencies.
* **rmmod <module>**:
  Unloads a module.
* **lsmod**:
  Lists loaded modules.
* **modinfo <module>**:
  Prints general information of a module.
* **dmesg**:
  Prints kernel log messages.


Modinfo example
===============

.. image:: img/modinfo.png
   :width: 15cm


Hello world
===========

A module example code:

.. code-block:: c

    #include <linux/init.h>
    #include <linux/module.h>

    static int __init hello_init(void) {
            printk(KERN_INFO "Hello World\n");
            return 0;
    }

    static void __exit hello_exit(void) {
            printk(KERN_INFO "Bye World\n");
    }

    module_init(hello_init);
    module_exit(hello_exit);


Compiling a Module
==================

* **out-of-tree**: Kernel source code is not required.
* **in-tree**: Code needs to be placed in the kernel's code tree.

Out-of-tree
===========

**No Kernel source code is needed**

Requires:

* **linux-headers-$(uname -r)**: so the modules can access functions from
  kernel.
* **Makefile**: echo "obj-m+=hello.o" > Makefile


In-tree
=======

* **Inside a sub-directory of the Linux kernel**. E.g.:

.. code-block::

  $ ls drivers/misc/hello/
  hello.c Makefile

* **Compiled with the Kernel code**

* Can be *loadable* or *built-in*.

Loadable module
===============

* **Can be loaded in run-time (same as out-of-tree)**

* Building as loadable:

.. code-block::

   $ cat drivers/misc/hello/Makefile
   obj-m+=hello.o


Built-in
========

* **Compiled code is loaded with the Kernel (at boot)**
* Building as built-in:

.. code-block::

   $ cat drivers/misc/hello/Makefile
   obj-y+=hello.o

Making it optional
==================

**Add variable for module/built-in**

* Building as a $(user choice)

.. code-block::

  $ cat drivers/misc/hello/Makefile
  obj-$(CONFIG_HELLO)+=hello.o


Making it optional
==================

.. image:: img/make-menuconfig.png
   :width: 15cm


.. raw:: pdf

   PageBreak coverPage

:cover-title:`Char driver example`
========================================

.. raw:: pdf

   PageBreak standardPage


Driver example code
===================

.. code-block:: c

    static ssize_t turn_on_led(struct file *file, char __user *buf,
                                size_t count, loff_t *offset)
    {
            make_led_turn_on();
            return 0;
    }

    struct file_operations fops = {
            .read = turn_on_led,
    };

    static int led_init(void)
    {
            register_chrdev(42, KBUILD_MODNAME, &fops);
            return 0;
    }


Example driver out-of-tree compilation
======================================

.. code-block::

   $ ls
   hello.c Makefile

.. code-block::

   $ make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

.. code-block::

   $ ls
   hello.c hello.ko hello.mod.c hello.mod.o hello.o Makefile modules.order
   Module.symvers

.. code-block::
   
   $ sudo insmod hello.ko


Device file example
===================

**$ mknod /dev/led c 42 0**

**$ cat /dev/led**

Led turns on!


We only saw part of the story
=============================

.. image:: img/diag_dd_vs_syscall.png
   :width: 14cm


License
=======

This work is licensed under a `Creative Commons "Attribution-ShareAlike 4.0
International"`_ license.

.. _Creative Commons "Attribution-ShareAlike 4.0 International":
   https://creativecommons.org/licenses/by-sa/4.0/deed.en


Bibliography
============

* Linux device drivers, 3rd edition. https://lwn.net/Kernel/LDD3/. Accessed:
  2019-04
