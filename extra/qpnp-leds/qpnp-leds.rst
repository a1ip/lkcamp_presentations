###########################################
Porting Nexus 5's flash LED driver upstream
###########################################

.. |arrow| replace:: →
.. role:: cover
.. role:: tiny-code
.. role:: emoji


.. space:: 150

.. class:: cover

   Nícolas F. R. A. Prado

   Jan 19, 2021

.. raw:: pdf

   PageBreak standardPage

A bit of context...
===================

* It all started when tony_ sent me a Nexus 5 to hack.

.. image:: img/nexus.jpg
   :width: 35%

.. _tony: https://andrealmeid.com/

A bit more context...
=====================

* ... And showed me nexus-5-upstream_: Lots of info about components on the
  Nexus 5 supported upstream!

.. image:: img/nexus-5-upstream.png

.. _nexus-5-upstream: https://masneyb.github.io/nexus-5-upstream/

Made an UART cable
==================

* Before developing anything, an UART cable was needed to debug.

* Followed nexus-5-upstream and the postmarketOS wiki and made an USB to audio
  jack cable for UART [3]:

.. image:: img/uart_cable.jpg
   :width: 50%

Nexus 5 TODO list
=================

* nexus-5-upstream had a TODO list. One of the items:

   The rear flashlight is supported in the downstream Linux kernel with the
   ``drivers/leds/leds-qpnp.c`` driver and referred to as torch in that code.
   Likely requires a new driver upstream.

* A simple LED, with code already done, although outdated... Seems like a great
  candidate!

  * Get driver code |arrow| update |arrow| test |arrow| profit!
    
The downstream driver [4]
=========================

* Over 3500 lines!! :emoji:`😱`

* Not all for the flash, though. It handled WLED, Flash/Torch, RGB, MPP and
  KPDBL.

* So, first step: remove all code not used for the flash LED.

Copying to the new driver
=========================

* Started copying the *probe* function, compiling, and checking the "missing
  definition" errors. For each:

  * If also for flash ("flash" or "torch" in the name): copy over
  * Else: just delete the reference

* Repeat until no more missing definitions

Solving compiling errors
========================

* Still a lot of other compiling errors

* **Remember**: downstream kernel: 3.4 (~2013); upstream kernel: 5.7.6. Lots of
  differences in interfaces.

* Patiently went through each one, understanding what changed, and fixing, until
  it compiled succesfully.

Adding the CONFIG and enabling the driver
=========================================

.. class:: tiny-code

   .. code:: diff
      :include: code/config2

.. class:: tiny-code

   .. code:: diff
      :include: code/config3

.. class:: tiny-code

   .. code:: diff
      :include: code/config


.. raw:: pdf

   PageBreak twocolumn


Adding the devicetree
=====================

* Copied from multiple dts files downstream
* To single dts upstream for now (easier to debug)

.. space:: 200

.. class:: tiny-code

   .. code:: diff
      :include: code/dts


.. raw:: pdf

   PageBreak standardPage


1st Test
========

* Compile
* Flash
* Test

* **Fail**: Driver wasn't being used

* I needed to better understand devicetree and driver binding!

"Solving Device Tree Issues" slides
===================================

* Read "Solving Device Tree Issues" [5].
  
* Used ``dt_node_info``:

.. code::
   :include: code/dtinfo

* Driver wasn't binding!

Device bus: platform vs spmi
============================

* After reading documentation and other drivers: Needed to register my driver on
  ``platform`` bus, not ``spmi``.

* But I used the ``spmi`` bus to access the flash LED registers through ``struct
  spmi_device *spmi_dev``...

* Read another driver's code and found an alternative. A generic address
  read/write mechanism. ``regmap`` to the rescue!

Commit: Use regmap
==================

.. class:: tiny-code

   .. code:: diff
      :include: code/regmap

Commit: Use platform bus
========================

.. code:: diff
   :include: code/platform_bus

(Not full patches)

2nd Test
========

* Compile
* Flash
* Test

* **Fail**: 

.. code:: dmesg
   :include: code/spmi_read_error

* Regulator nodes (copied from downstream) weren't working. Needed to understand
  upstream differences.

Trying downstream out
=====================

* Flashed downstream kernel to see double-check if everything was working

* Explored *sysfs*

* Used ``echo 1 > /sys/class/leds/led\:flash_torch/brightness`` to turn flash
  on and worked no problem.

* Confirmed that the regulator node on the devicetree had its ``status`` going
  to ``enabled`` when the LED turned on.

Detective work
==============

* Downstream worked like I thought and upstream failed, so I should use the
  equivalent regulator node upstreamed (if it was already upstream).

* Begins the search for the correspondence between regulator nodes downstream
  and upstream.

* Started investigating using

  * downstream regulator nodes in the devicetree
  * upstream driver for regulator, ``qcom_spmi-regulator``, and its dtbinding

Discovering pm8941_boost's real identity
========================================

* The first regulator, ``pm8941_boost`` has ``a000`` as its address:

.. code::

  pm8941_boost: regulator@a000 {

* Searching for that in the ``qcom_spmi-regulator`` driver yields:

.. code:: c
   :include: code/regulator_driver_a000

Discovering pm8941_boost's real identity (2)
============================================

* And if we ``git blame`` that last line:

.. code::
   :include: code/blame_a000

Discovering pm8941_boost's real identity (3)
============================================

* Finally, searching for ``s4`` in the upstream devicetree files, we find in
  ``arch/arm/boot/dts/qcom-pm8941.dtsi`` which is included by the hammerhead dts,
  the following:

.. code:: devicetree
   :include: code/dts_s4

pm8941_boost == pm8941_5v
=========================

* Hooray! So it turns out that in the upstream devicetree, I needed to use the
  ``pm8941_5v`` regulator node instead of the ``pm8941_boost`` used downstream.

Discovering pm8941_chg_boost's real identity
============================================

* The other regulator is ``pm8941_chg_boost``.

* The upstream devicetree has a "charger" node:

.. class:: tiny-code

   .. code:: devicetree
      :include: code/charger_dts 

Discovering pm8941_chg_boost's real identity (2)
================================================

* And the ``usb-otg-in-supply`` field points to ``pm8941_5vs1`` which is another
  regulator node, right beside the last one we used:

.. code:: devicetree
   :include: code/charger_reg

* Let's try ``pm8941_5vs1`` instead of ``pm8941_chg_boost``!


.. raw:: pdf

   PageBreak twocolumn


Commit: Right regulators in dts
===============================

.. class:: tiny-code

   .. code:: diff
      :include: code/diff_regs


.. raw:: pdf

   PageBreak standardPage


3rd Test
========

* Compile, Flash, Test...

* **Fail**: 

.. class:: tiny-code

   .. code:: dmesg
      :include: code/error_reg

* Setup was now OK, but register read/write were all failing...

My own bug!
===========

* Started reviewing the ``probe()`` function

* Sprinkled ``pr_debug()`` all over the code

* Noticed the ``reg`` value was ``0`` instead of ``0xd300``.

.. class:: tiny-code

   .. code:: c
      :include: code/reg_code

* The reg value was saved to ``rc`` and overwritten by the return value...

* This bug I introduced myself! Oops :emoji:`😅`

Commit: Fix reg value being overwritten
=======================================

.. class:: tiny-code

   .. code:: diff
      :include: code/reg_fix

4th Test
========

* Compile
* Flash
* Test

* ...

It works!
=========

.. image:: img/flash_off.png
   :width: 45%

.. image:: img/flash_on.png
   :width: 45%

Time for the mailing list!
==========================

* Rebased all work into three nice patches:

  * leds: Add driver for QPNP flash led
  * ARM: qcom_defconfig: Add QPNP flash LED support
  * ARM: dts: qcom: msm8974-hammerhead: Add support for the flash LED

* Send as an RFC patch to get as much feedback as possible

* ``git send-email``!

Feedback
========

* Mailing list:

  * Missing dtbinding!

  * Use the LED class framework (merged since 2015)

  * Rename to ``qcom_spmi_flash`` instead of ``qpnp_leds`` (because other QPNP
    LED types are already upstream)

* Tony:

  * Restore (and update) the copyright that I had accidentaly removed!

Recent work
===========

* Added dtbinding
* Major refactoring of the code
* Partially implemented the LED class framework

----

* Last commit on late November, but I want to get back at it in the next couple
  days!

TODO
====

* Finish implementing the LED class framework

----

* Send it for Non-RFC v1 (v2?).
* Address feedback and resend until merged.


Thanks
======

Tony, for the phone, of course!!

----

Slides proudly generated from rST source using rst2pdf_.

.. _rst2pdf: https://rst2pdf.org/

Last words
==========

* Presentation based on my blog post [1].
* Full patches can be seen in my Linux tree [2].

License
=======

This work is licensed under a `Creative Commons "Attribution-ShareAlike 4.0
International"`_ license.

.. _Creative Commons "Attribution-ShareAlike 4.0 International":
   https://creativecommons.org/licenses/by-sa/4.0/deed.en

References
==========

[1] Porting a flash LED driver upstream.
https://nfraprado.gitlab.io/porting-a-flash-led-driver-upstream.html

[2] My Linux tree.
https://gitlab.com/nfraprado/linux

[3] Making an UART cable for the Nexus 5.
https://nfraprado.gitlab.io/making-an-uart-cable-for-the-nexus-5.html

[4] Downstream driver.
https://github.com/AICP/kernel_lge_hammerhead/blob/n7.1/drivers/leds/leds-qpnp.c

[5] Solving Device Tree Issues.
https://elinux.org/images/0/04/Dt_debugging_elce_2015_151006_0421.pdf
