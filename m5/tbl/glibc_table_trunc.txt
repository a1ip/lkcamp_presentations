\%rax	& System call	        & \%rdi	                & \%rsi	                        & \%rdx	\\
\hline  &&&&\\
0	& sys\_read	        & unsigned int fd	& char *buf	                & size\_t count \\
1	& sys\_write	        & unsigned int fd	& const char *buf	        & size\_t count	\\
2	& sys\_open	        & const char *filename	& int flags	                & int mode \\
3	& sys\_close	        & unsigned int fd	&& \\
4	& sys\_stat	        & const char *filename	& struct stat *statbuf		& \\
5	& sys\_fstat	        & unsigned int fd	& struct stat *statbuf		& \\
6	& sys\_lstat	        & fconst char *filename	& struct stat *statbuf		& \\
7	& sys\_poll	        & struct poll\_fd *ufds	& unsigned int nfds	        & long timeout\_msecs \\
8	& sys\_lseek	        & unsigned int fd	& off\_t offset	                & unsigned int origin \\
...     &&&&\\
10	& sys\_mprotect	        & unsigned long start	& size\_t len	                & unsigned long prot \\
11	& sys\_munmap	        & unsigned long addr	& size\_t len			& \\
12	& sys\_brk	        & unsigned long brk	&& \\
...     &&&&\\
15	& sys\_rt\_sigreturn	& unsigned long \_\_unused&& \\
16	& sys\_ioctl	        & unsigned int fd	& unsigned int cmd	        & unsigned long arg \\
